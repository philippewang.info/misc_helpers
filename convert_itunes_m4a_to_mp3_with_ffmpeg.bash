#!/bin/bash

set -xe

combined=${1:-'combined'}

for i in *.m4a ; do mv "$i" "$(sed -e 's/ /_/g' <<<"$i")" ; done

mkdir newfiles && for f in *.m4a; do ffmpeg -i "$f" -codec:v copy -codec:a libmp3lame -q:a 2 newfiles/"${f%.m4a}.mp3"; done

ffmpeg -i "concat:$(echo newfiles/*.mp3|sed -e 's/ /|/g')" -acodec  copy "${combined}.mp3"

rm -fr newfiles
